package eu.fbk.threedom.jlibtiff;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lake on 21/01/16.
 */
public class TiffWriteTest {

    private TIFF tiff;

    @Before
    public void setup(){
        tiff = JLibTiff.TIFFOpen("build/testOut.tif", "w");
    }

    @Test
    public void testSetFieldShort() throws TagNotFoundException {
        tiff.setFieldShort(TiffTag.BITSPERSAMPLE.getTag(), (short) 8);
        assertEquals((short)8, tiff.getFieldShort(TiffTag.BITSPERSAMPLE.getTag()));
    }

    @Test
    public void testSetFieldShorts() throws TagNotFoundException {
        tiff.setFieldShort(TiffTag.SAMPLESPERPIXEL.getTag(), (short) 7);
        tiff.setFieldShorts(TiffTag.EXTRASAMPLES.getTag(), new short[]{0, 0, 0, 0});

        short[] extraSamples = tiff.getFieldShorts(TiffTag.EXTRASAMPLES.getTag());
        assertArrayEquals(new short[]{0, 0, 0, 0}, extraSamples);
    }

    @Test
    public void testSetFieldInt() throws TagNotFoundException {
        tiff.setFieldInt(TiffTag.IMAGEDEPTH.getTag(), 34);
        assertEquals(34, tiff.getFieldInt(TiffTag.IMAGEDEPTH.getTag()));
    }

    @Test
    public void testSetFieldInts() throws TagNotFoundException {
        tiff.setFieldInts(TiffTag.SUBIFD.getTag(), new int[]{0});

        int[] subifds = tiff.getFieldInts(TiffTag.SUBIFD.getTag());
        assertArrayEquals(new int[]{0}, subifds);
    }

    @Test
    public void testSetFieldFloat() throws TagNotFoundException {
        tiff.setFieldFloat(TiffTag.XPOSITION.getTag(), 0.456f);
        assertEquals(0.456f, tiff.getFieldFloat(TiffTag.XPOSITION.getTag()), 0.001f);
    }

    @Test
    public void testSetFieldFloats() throws TagNotFoundException {
        tiff.setFieldFloats(TiffTag.WHITEPOINT.getTag(), new float[]{0.3f, 0.45f});

        float[] vals = tiff.getFieldFloats(TiffTag.WHITEPOINT.getTag(), 2);
        assertArrayEquals(new float[]{0.3f, 0.45f}, vals, 0.001f);
    }

    @Test
    public void testSetFieldDouble() throws TagNotFoundException {
        tiff.setFieldDouble(TiffTag.SMAXSAMPLEVALUE.getTag(), 0.456);
        assertEquals(0.456, tiff.getFieldDouble(TiffTag.SMAXSAMPLEVALUE.getTag()), 0.0001f);
    }

    @Test
    public void testSetFieldString() throws TagNotFoundException {
        tiff.setFieldString(TiffTag.SOFTWARE.getTag(), "3DOM jlibtiff");
        assertEquals("3DOM jlibtiff", tiff.getFieldString(TiffTag.SOFTWARE.getTag()));
    }

    @After
    public void testClose(){
        tiff.close();
    }
}
