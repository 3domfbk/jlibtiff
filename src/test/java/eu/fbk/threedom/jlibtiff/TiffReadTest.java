package eu.fbk.threedom.jlibtiff;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lake on 20/01/16.
 */
public class TiffReadTest {

    private TIFF tiff;

    @Before
    public void setup(){
	String path = getClass().getClassLoader().getResource("test1.tif").getPath();
        tiff = JLibTiff.TIFFOpen(path, "r");
    }

    @Test
    public void testTIFFOpen() throws Exception {
        assertNotNull(tiff);
    }

    @Test(expected = TagNotFoundException.class)
    public void testIntTagNotFound() throws Exception {
        tiff.getFieldInt(TiffTag.GROUP4OPTIONS.getTag());
    }

    @Test(expected = TagNotFoundException.class)
    public void testIntsTagNotFound() throws Exception {
        tiff.getFieldInts(TiffTag.SUBIFD.getTag());
    }

    @Test(expected = TagNotFoundException.class)
    public void testShortTagNotFound() throws Exception {
        tiff.getFieldShort(TiffTag.THRESHHOLDING.getTag());
    }

    @Test(expected = TagNotFoundException.class)
    public void testShortsTagNotFound() throws Exception {
        tiff.getFieldShorts(TiffTag.COLORMAP.getTag());
    }

    @Test(expected = TagNotFoundException.class)
    public void testStringTagNotFound() throws Exception {
        tiff.getFieldString(TiffTag.COPYRIGHT.getTag());
    }

    @Test
    public void testGetFieldShort() throws Exception {
        short bitsPerSample = tiff.getFieldShort(TiffTag.BITSPERSAMPLE.getTag());
        assertTrue(bitsPerSample == 8);
    }

    @Test
    public void testGetFieldShorts() throws Exception {
        short[] extraSamples = tiff.getFieldShorts(TiffTag.EXTRASAMPLES.getTag());
        assertArrayEquals(new short[]{0, 0, 0, 0}, extraSamples);
    }

    @Test
    public void testGetFieldInt() throws Exception {
        int imageWidth = tiff.getFieldInt(TiffTag.IMAGEWIDTH.getTag());
        assertTrue(imageWidth == 2480);
    }

    @Test
    public void testGetFieldIntsKnownSize() throws Exception {
        int strips = tiff.numberOfStrips();
        int[] stripBytes = tiff.getFieldInts(TiffTag.STRIPBYTESCOUNT.getTag(), strips);
        assertEquals(strips, stripBytes.length);
    }

    @Test(expected = TagNotFoundException.class)
    public void testGetFieldIntsKnownSizeNotFound() throws Exception {
        tiff.getFieldInts(TiffTag.TILEDEPTH.getTag(), 1);
    }

    @Test(expected = TagNotFoundException.class)
    public void testGetFieldShortsKnownSize() throws Exception {
        tiff.getFieldShorts(TiffTag.TRANSFERFUNCTION.getTag(), 3);
    }

    @Test
    public void testGetFieldString() throws Exception {
        String software = tiff.getFieldString(TiffTag.SOFTWARE.getTag());
        assertEquals("Adobe Photoshop CS6 (Windows)", software);
    }

    @Test
    public void testNumberOfStrips() throws Exception {
        int strips = tiff.numberOfStrips();
        assertEquals(3508, strips);
    }

    @Test
    public void testGetFieldFloat() throws Exception {
        float xpos = tiff.getFieldFloat(TiffTag.XRESOLUTION.getTag());
        assertEquals(300f, xpos, 0.001f);
    }

    @Test(expected = TagNotFoundException.class)
    public void testGetFieldFloats() throws Exception {
        tiff.getFieldFloats(TiffTag.PRIMARYCHROMATICITIES.getTag(), 6);
    }

    @Test(expected = TagNotFoundException.class)
    public void testGetFieldDouble() throws Exception {
        tiff.getFieldDouble(TiffTag.SMAXSAMPLEVALUE.getTag());
    }

    @Test(expected = TagNotFoundException.class)
    public void testGetFieldDoubles() throws Exception {
        tiff.getFieldDoubles(TiffTag.STONITS.getTag(), 2);
    }

    @Test
    public void testStripSize() throws Exception {
        assertEquals(17360, tiff.stripSize());
    }

    @Test(timeout = 1000)
    public void testReadImageData() throws Exception {
        byte[] data = tiff.readImageData();
        assertEquals(60898880, data.length);
    }

    @Test
    public void testReadDirectory() throws Exception {
        assertFalse(tiff.readDirectory());
    }

    @After
    public void testClose(){
        tiff.close();
    }
}
