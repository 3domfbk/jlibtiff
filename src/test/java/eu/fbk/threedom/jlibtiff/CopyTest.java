package eu.fbk.threedom.jlibtiff;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by lake on 21/01/16.
 *
 * TIFF image copy test
 */
public class CopyTest {

    @Test
    public void testCopy() throws TagNotFoundException {
        // Read input image
	String path = getClass().getClassLoader().getResource("test1.tif").getPath();
        TIFF tiff = JLibTiff.TIFFOpen(path, "r");

        short spp = tiff.getFieldShort(TiffTag.SAMPLESPERPIXEL.getTag());
        int width = tiff.getFieldInt(TiffTag.IMAGEWIDTH.getTag());
        int height = tiff.getFieldInt(TiffTag.IMAGELENGTH.getTag());
        short[] extraSamples = tiff.getFieldShorts(TiffTag.EXTRASAMPLES.getTag());
        byte[] data = tiff.readImageData();

        tiff.close();

        // Write output image
        tiff = JLibTiff.TIFFOpen("build/copyOut.tif", "w");
        tiff.setFieldInt(TiffTag.SUBFILETYPE.getTag(), 0);
        tiff.setFieldInt(TiffTag.IMAGEWIDTH.getTag(), width);
        tiff.setFieldInt(TiffTag.IMAGELENGTH.getTag(), height);
        tiff.setFieldInt(TiffTag.PLANARCONFIG.getTag(), TiffTag.PLANARCONFIG_CONTIG);
        tiff.setFieldInt(TiffTag.SAMPLESPERPIXEL.getTag(), spp);
        tiff.setFieldShorts(TiffTag.EXTRASAMPLES.getTag(), extraSamples);
        tiff.setFieldInt(TiffTag.BITSPERSAMPLE.getTag(), 8);
        tiff.setFieldInt(TiffTag.ROWSPERSTRIP.getTag(), height);
        tiff.setFieldInt(TiffTag.PHOTOMETRIC.getTag(), TiffTag.PHOTOMETRIC_RGB);
        tiff.setFieldInt(TiffTag.ORIENTATION.getTag(), TiffTag.ORIENTATION_TOPLEFT);

        tiff.writeEncodedStrip(0, data);
        tiff.close();

        // Read it back
        tiff = JLibTiff.TIFFOpen("build/copyOut.tif", "r");

        short spp2 = tiff.getFieldShort(TiffTag.SAMPLESPERPIXEL.getTag());
        int width2 = tiff.getFieldInt(TiffTag.IMAGEWIDTH.getTag());
        int height2 = tiff.getFieldInt(TiffTag.IMAGELENGTH.getTag());
        short[] extraSamples2 = tiff.getFieldShorts(TiffTag.EXTRASAMPLES.getTag());
        byte[] data2 = tiff.readImageData();

        assertEquals(spp, spp2);
        assertEquals(width, width2);
        assertEquals(height, height2);
        assertArrayEquals(extraSamples, extraSamples2);
        assertArrayEquals(data, data2);

        tiff.close();
    }
}
