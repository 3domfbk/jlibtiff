package eu.fbk.threedom.jlibtiff;

import com.badlogic.gdx.jnigen.JniGenSharedLibraryLoader;

/**
 * Created by lake on 19/01/16.
 */
public class JLibTiff {
    static {
        new JniGenSharedLibraryLoader().load("jtiff");
        //System.loadLibrary("jtiff");
    }

    /*JNI
	    #include <tiffio.h>
	 */


    /**
     * Opens a tiff file
     * @param path The file path
     * @param mode The mode ("r" or "w")
     * @return The TIFF image, or null in case of error
     */
    public static TIFF TIFFOpen(String path, String mode){
        long handle = TIFFOpenN(path, mode);
        if( handle == 0L ) return null;
        return new TIFF(handle);
    }

    private static native long TIFFOpenN(String path, String mode); /*
        return (jlong)TIFFOpen(path, mode);
    */
}
