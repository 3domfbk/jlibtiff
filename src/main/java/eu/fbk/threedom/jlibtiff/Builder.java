package eu.fbk.threedom.jlibtiff;

import com.badlogic.gdx.jnigen.*;

/**
 * Created by lake on 20/01/16.
 */
public class Builder {

    static public void main (String[] args) throws Exception {

        // Generate jni
        NativeCodeGenerator jnigen = new NativeCodeGenerator();
        jnigen.generate("src/main/java", "build/classes/main", "jni");

        // Build for linux
        BuildTarget linux64 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.Linux, true);
        linux64.libraries += "-ltiff -ljpeg";
        BuildTarget linux32 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.Linux, false);
        linux32.libraries += "-ltiff -ljpeg";
        BuildTarget win64 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.Windows, true);
        win64.libraries += "-ltiff -ljpeg";
        BuildTarget win32 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.Windows, false);
        win32.libraries += "-ltiff -ljpeg";
        BuildTarget osx64 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.MacOsX, true);
        osx64.libraries += "-ltiff -ljpeg";
        BuildTarget osx32 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.MacOsX, false);
        osx32.libraries += "-ltiff -ljpeg";
        BuildTarget ios32 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.IOS, false);
        ios32.libraries += "-ltiff -ljpeg";
        BuildTarget ios64 = BuildTarget.newDefaultTarget(BuildTarget.TargetOs.IOS, true);
        ios64.libraries += "-ltiff -ljpeg";

        new AntScriptGenerator().generate(new BuildConfig("jtiff"), linux64, linux32, win64, win32, osx64, osx32, ios32, ios64);
        BuildExecutor.executeAnt("jni/build-linux64.xml", "-v -Dhas-compiler=true clean");
        BuildExecutor.executeAnt("jni/build-linux64.xml", "-v -Dhas-compiler=true");
        BuildExecutor.executeAnt("jni/build-linux32.xml", "-v -Dhas-compiler=true clean");
        BuildExecutor.executeAnt("jni/build-linux32.xml", "-v -Dhas-compiler=true");
        BuildExecutor.executeAnt("jni/build-windows64.xml", "-v -Dhas-compiler=true clean");
        BuildExecutor.executeAnt("jni/build-windows64.xml", "-v -Dhas-compiler=true");
        BuildExecutor.executeAnt("jni/build-windows32.xml", "-v -Dhas-compiler=true clean");
        BuildExecutor.executeAnt("jni/build-windows32.xml", "-v -Dhas-compiler=true");
//        BuildExecutor.executeAnt("jni/build-macosx64.xml", "-v -Dhas-compiler=true clean");
//        BuildExecutor.executeAnt("jni/build-macosx64.xml", "-v -Dhas-compiler=true");
//        BuildExecutor.executeAnt("jni/build-macosx32.xml", "-v -Dhas-compiler=true clean");
//        BuildExecutor.executeAnt("jni/build-macosx32.xml", "-v -Dhas-compiler=true");
//        BuildExecutor.executeAnt("jni/build-ios32.xml", "-v -Dhas-compiler=true clean");
//        BuildExecutor.executeAnt("jni/build-ios32.xml", "-v -Dhas-compiler=true");
//        BuildExecutor.executeAnt("jni/build-ios64.xml", "-v -Dhas-compiler=true clean");
//        BuildExecutor.executeAnt("jni/build-ios64.xml", "-v -Dhas-compiler=true");

        BuildExecutor.executeAnt("jni/build.xml", "pack-natives -v");
    }
}
