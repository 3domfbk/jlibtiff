package eu.fbk.threedom.jlibtiff;

/**
 * Created by lake on 19/01/16.
 */
public enum TiffTag {
    SUBFILETYPE(254),
    OSUBFILETYPE(255),
    IMAGEWIDTH(256),
    IMAGELENGTH(257),
    BITSPERSAMPLE(258),
    COMPRESSION(259),
    PHOTOMETRIC(262),
    THRESHHOLDING(263),
    CELLWIDTH(264),
    CELLLENGTH(265),
    FILLORDER(266),
    DOCUMENTNAME(267),
    IMAGEDESCRIPTION(270),
    MAKE(271),
    MODEL(272),
    STRIPOFFSETS(273),
    ORIENTATION(274),
    SAMPLESPERPIXEL(277),
    ROWSPERSTRIP(278),
    STRIPBYTESCOUNT(279),
    MINSAMPLEVALUE(280),
    MAXSAMPLEVALUE(281),
    XRESOLUTION(282),
    YRESOLUTION(283),
    PLANARCONFIG(284),
    PAGENAME(285),
    XPOSITION(286),
    YPOSITION(287),
    FREEOFFSETS(288),
    FREEBYTECOUNTS(289),
    GRAYRESPONSEUNIT(290),
    GRAYRESPONSECURVE(291),
    GROUP3OPTIONS(292),
    T4OPTIONS(292),
    GROUP4OPTIONS(293),
    T6OPTIONS(293),
    RESOLUTIONUNIT(296),
    PAGENUMBER(297),
    COLORRESPONSEUNIT(300),
    TRANSFERFUNCTION(301),
    SOFTWARE(305),
    DATETIME(306),
    ARTIST(315),
    HOSTCOMPUTER(316),
    PREDICTOR(317),
    WHITEPOINT(318),
    PRIMARYCHROMATICITIES(319),
    COLORMAP(320),
    SUBIFD(330),
    EXTRASAMPLES(338),
    SAMPLEFORMAT(339),

    SMAXSAMPLEVALUE(341),

    YCBCRPOSITIONING(531),

    IMAGEDEPTH(32997),
    TILEDEPTH(32998),

    TIFFTAG_PIXAR_IMAGEFULLWIDTH(33300),
    TIFFTAG_PIXAR_IMAGEFULLLENGTH(33301),

    COPYRIGHT(33432),

    STONITS(37439)
    ;

    public static final int PLANARCONFIG_CONTIG = 1;
    public static final int PLANARCONFIG_SEPARATE = 2;

    public static final int PHOTOMETRIC_MINISWHITE = 0;
    public static final int PHOTOMETRIC_MINISBLACK = 1;
    public static final int PHOTOMETRIC_RGB = 2;
    public static final int PHOTOMETRIC_PALETTE = 3;
    public static final int PHOTOMETRIC_MASK = 4;
    public static final int PHOTOMETRIC_SEPARATED = 5;

    public static final int ORIENTATION_TOPLEFT = 1;
    public static final int ORIENTATION_TOPRIGHT = 2;
    public static final int ORIENTATION_BOTRIGHT = 3;
    public static final int ORIENTATION_BOTLEFT = 4;
    public static final int ORIENTATION_LEFTTOP = 5;
    public static final int ORIENTATION_RIGHTTOP = 6;
    public static final int ORIENTATION_RIGHTBOT = 7;
    public static final int ORIENTATION_LEFTBOT = 8;

    public static final int SAMPLEFORMAT_UINT = 1;
    public static final int SAMPLEFORMAT_INT = 2;
    public static final int SAMPLEFORMAT_IEEEFP = 3;
    public static final int SAMPLEFORMAT_VOID = 4;
    public static final int SAMPLEFORMAT_COMPLEXINT = 5;
    public static final int SAMPLEFORMAT_COMPLEXIEEEFP = 6;



    private int tag;
    TiffTag(int tag){
        this.tag = tag;
    }

    public int getTag() {
        return tag;
    }
}
