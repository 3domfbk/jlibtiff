package eu.fbk.threedom.jlibtiff;

/**
 * Created by lake on 20/01/16.
 *
 * Wraps libtiff's TIFF class functionality
 */
public class TIFF extends Handle {
    /*JNI
	    #include <tiffio.h>
	    #include <string.h>
	 */

    TIFF(long handle) {
        super(handle);
    }

    /**
     * @return Number of strips
     */
    public int numberOfStrips(){
        return numberOfStrips(handle);
    }

    /**
     * @return Strip size
     */
    public int stripSize() {
        return stripSize(handle);
    }

    /**
     * Read image project from tiff image as byte array.
     * Automatically determines whether the image is in strips
     * or tiles, and behaves properly
     * @return Image project as stored in the TIFF image
     */
    public byte[] readImageData(){
        return readImageData(handle);
    }

    private native byte[] readImageData(long handle); /*
        TIFF* tif = (TIFF*)handle;
        uint32 stripSize = TIFFStripSize(tif);
        uint32 stripsNum = TIFFNumberOfStrips(tif);
        uint32 size = stripSize*stripsNum;

        unsigned char* project = (unsigned char*)_TIFFmalloc(size);

        // Read from strips
        if( stripsNum > 0 ){
            // Read all strips into memory
            for (int strip = 0; strip < stripsNum; strip++) {
                TIFFReadEncodedStrip(tif, strip, project + (strip*stripSize), (tsize_t)stripSize);
            }

            // Make java array
            jbyteArray ret = env->NewByteArray(size);
            env->SetByteArrayRegion (ret, 0, size, (const jbyte*)project);

            _TIFFfree(project);
            return ret;
        }

        // Read from tiles
        else {
            // TODO

            _TIFFfree(project);
            return NULL;
        }
    */

    /**
     * Writes an encoded (?) strip into the TIFF image
     * @param strip Strip number
     * @param data Encoded (?) project
     */
    public void writeEncodedStrip(int strip, byte[] data){
        writeEncodedStrip(handle, strip, data, data.length);
    }

    private native void writeEncodedStrip(long handle, int strip, byte[] data, int length); /*
        TIFFWriteEncodedStrip((TIFF*)handle, strip, data, length);
    */

    /**
     * Go to next directory
     */
    public boolean readDirectory(){
        return readDirectory(handle);
    }

    private native boolean readDirectory(long handle); /*
        return TIFFReadDirectory((TIFF*)handle);
    */

    /**
     * Write to next directory
     */
    public void writeDirectory(){
        writeDirectory(handle);
    }

    private native void writeDirectory(long handle); /*
        TIFFWriteDirectory((TIFF*)handle);
    */

    /**
     * Closes the TIFF
     */
    public void close(){
        close(handle);
    }

    private native void close(long handle); /*
        TIFFClose((TIFF*)handle);
    */

    /**
     * Gets a field with single short value
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public short getFieldShort(int tag) throws TagNotFoundException {
        short res = getFieldShort(handle, tag);
        if( res == -9876 ) throw new TagNotFoundException();
        return res;
    }

    /**
     * Gets a field whose value is an array of shorts
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public short[] getFieldShorts(int tag) throws TagNotFoundException {
        int size = getArraySize(handle, tag);
        if( size == -1 ) throw new TagNotFoundException();

        short[] res = new short[size];

        long valueHandle = getArrayHandle(handle, tag);
        for( int i=0; i<res.length; i++ ){
            res[i] = getShortArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Gets a field whose value is an array of ints of a known size
     * @param tag TIFF tag
     * @param elements The known number of elements
     * @return The value of the tag
     */
    public short[] getFieldShorts(int tag, int elements) throws TagNotFoundException {
        short[] res = new short[elements];

        long valueHandle = getRawHandle(handle, tag);
        if(valueHandle == 0L) throw new TagNotFoundException();

        for( int i=0; i<res.length; i++ ){
            res[i] = getShortArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Gets a field with single int value
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public int getFieldInt(int tag) throws TagNotFoundException {
        int res = getFieldInt(handle, tag);
        if( res == -9876 ) throw new TagNotFoundException();
        return res;
    }

    /**
     * Gets a field whose value is an array of ints
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public int[] getFieldInts(int tag) throws TagNotFoundException {
        int size = getArraySize(handle, tag);
        if( size == -1 ) throw new TagNotFoundException();

        int[] res = new int[size];

        long valueHandle = getArrayHandle(handle, tag);
        for( int i=0; i<res.length; i++ ){
            res[i] = getIntArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Gets a field whose value is an array of ints of a known size
     * @param tag TIFF tag
     * @param elements The known number of elements
     * @return The value of the tag
     */
    public int[] getFieldInts(int tag, int elements) throws TagNotFoundException {
        int[] res = new int[elements];

        long valueHandle = getRawHandle(handle, tag);
        if(valueHandle == 0L) throw new TagNotFoundException();

        for( int i=0; i<res.length; i++ ){
            res[i] = getIntArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Gets a field with string value
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public String getFieldString(int tag) throws TagNotFoundException {
        int length = getStringLength(handle, tag);
        if( length == -1 ) throw new TagNotFoundException();

        char[] chars = new char[length];

        long valueHandle = getRawHandle(handle, tag);
        for( int i=0; i<chars.length; i++ ){
            chars[i] = getCharArrayValue(valueHandle, i);
        }
        return new String(chars);
    }

    /**
     * Gets a field with single float value
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public float getFieldFloat(int tag) throws TagNotFoundException {
        float res = getFieldFloat(handle, tag);
        if( res == -9876 ) throw new TagNotFoundException();
        return res;
    }

    /**
     * Gets a field whose value is an array of floats of a known size
     * @param tag TIFF tag
     * @param elements The known number of elements
     * @return The value of the tag
     */
    public float[] getFieldFloats(int tag, int elements) throws TagNotFoundException {
        float[] res = new float[elements];

        long valueHandle = getRawHandle(handle, tag);
        if(valueHandle == 0L) throw new TagNotFoundException();

        for( int i=0; i<res.length; i++ ){
            res[i] = getFloatArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Gets a field with single double value
     * @param tag TIFF tag
     * @return The value of the tag
     */
    public double getFieldDouble(int tag) throws TagNotFoundException {
        double res = getFieldDouble(handle, tag);
        if( res == -9876 ) throw new TagNotFoundException();
        return res;
    }

    /**
     * Gets a field whose value is an array of floats of a known size
     * @param tag TIFF tag
     * @param elements The known number of elements
     * @return The value of the tag
     */
    public double[] getFieldDoubles(int tag, int elements) throws TagNotFoundException {
        double[] res = new double[elements];

        long valueHandle = getRawHandle(handle, tag);
        if(valueHandle == 0L) throw new TagNotFoundException();

        for( int i=0; i<res.length; i++ ){
            res[i] = getDoubleArrayValue(valueHandle, i);
        }

        return res;
    }

    /**
     * Sets a single short value
     * @param tag The TIFF tag
     * @param value The value
     */
    public void setFieldShort(int tag, short value){
        setFieldShort(handle, tag, value);
    }

    /**
     * Sets an array of short values
     * @param tag The TIFF tag
     * @param value The values
     */
    public void setFieldShorts(int tag, short[] value){
        setFieldShorts(handle, tag, value, value.length);
    }

    /**
     * Sets a single int value
     * @param tag The TIFF tag
     * @param value The value
     */
    public void setFieldInt(int tag, int value){
        setFieldInt(handle, tag, value);
    }

    /**
     * Sets an array of int values
     * @param tag The TIFF tag
     * @param value The values
     */
    public void setFieldInts(int tag, int[] value){
        setFieldInts(handle, tag, value, value.length);
    }

    /**
     * Sets a single float value
     * @param tag The TIFF tag
     * @param value The value
     */
    public void setFieldFloat(int tag, float value){
        setFieldFloat(handle, tag, value);
    }

    /**
     * Sets an array of float values
     * @param tag The TIFF tag
     * @param value The values
     */
    public void setFieldFloats(int tag, float[] value){
        setFieldFloats(handle, tag, value);
    }

    /**
     * Sets a single double value
     * @param tag The TIFF tag
     * @param value The value
     */
    public void setFieldDouble(int tag, double value){
        setFieldDouble(handle, tag, value);
    }

    /**
     * Sets an array of double values
     * @param tag The TIFF tag
     * @param value The values
     */
    public void setFieldDoubles(int tag, double[] value){
        setFieldDoubles(handle, tag, value);
    }

    /**
     * Sets a string value
     * @param tag The TIFF tag
     * @param value The values
     */
    public void setFieldString(int tag, String value){
        setFieldString(handle, tag, value);
    }

    private native void setFieldString(long handle, int tag, String value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldDouble(long handle, int tag, double value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldDoubles(long handle, int tag, double[] value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldFloat(long handle, int tag, float value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldFloats(long handle, int tag, float[] value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldInt(long handle, int tag, int value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native void setFieldInts(long handle, int tag, int[] value, int count); /*
        TIFFSetField((TIFF*)handle, tag, count, value);
    */

    private native void setFieldShorts(long handle, int tag, short[] value, int count); /*
        TIFFSetField((TIFF*)handle, tag, count, value);
    */

    private native void setFieldShort(long handle, int tag, short value); /*
        TIFFSetField((TIFF*)handle, tag, value);
    */

    private native int numberOfStrips(long handle); /*
        return TIFFNumberOfStrips((TIFF*)handle);
    */

    private native int stripSize(long handle); /*
        return TIFFStripSize((TIFF*)handle);
    */

    private native int getArraySize(long handle, int tag); /*
        uint16 size;
        void* dummy;
        if( !TIFFGetField((TIFF*)handle, tag, &size, &dummy) ) return -1;
        return size;
    */

    private native long getArrayHandle(long handle, int tag); /*
        uint16 size;
        void* dummy;
        TIFFGetField((TIFF*)handle, tag, &size, &dummy);
        return (jlong)dummy;
    */

    private native short getFieldShort(long handle, int tag); /*
        uint16 res;
        if( !TIFFGetField((TIFF*)handle, tag, &res) ) return -9876;
        return res;
    */

    private native short getShortArrayValue(long valueHandle, int pos); /*
        return ((uint16*)valueHandle)[pos];
    */

    private native int getFieldInt(long handle, int tag); /*
        uint32 res;
        if( !TIFFGetField((TIFF*)handle, tag, &res) ) return -9876;
        return res;
    */

    private native int getIntArrayValue(long valueHandle, int pos); /*
        return ((uint32*)valueHandle)[pos];
    */

    private native long getRawHandle(long handle, int tag); /*
        char* str;
        if( !TIFFGetField((TIFF*)handle, tag, &str) ) return 0L;
        return (jlong)str;
    */

    private native int getStringLength(long handle, int tag); /*
        char* str;
        if( !TIFFGetField((TIFF*)handle, tag, &str) ) return -1;
        return strlen(str);
    */

    private native char getCharArrayValue(long valueHandle, int pos); /*
        return ((char*)valueHandle)[pos];
    */

    private native float getFieldFloat(long handle, int tag); /*
        float res;
        if( !TIFFGetField((TIFF*)handle, tag, &res) ) return -9876;
        return res;
    */

    private native float getFloatArrayValue(long valueHandle, int pos); /*
        return ((float*)valueHandle)[pos];
    */

    private native float getFieldDouble(long handle, int tag); /*
        double res;
        if( !TIFFGetField((TIFF*)handle, tag, &res) ) return -9876;
        return res;
    */

    private native float getDoubleArrayValue(long valueHandle, int pos); /*
        return ((double*)valueHandle)[pos];
    */
}
